<FUNCTION>
<NAME>sort_configs</NAME>
<RETURNS>int  </RETURNS>
gconstpointer a, gconstpointer b
</FUNCTION>
<FUNCTION>
<NAME>get_options</NAME>
<RETURNS>GList  *</RETURNS>

</FUNCTION>
<FUNCTION>
<NAME>pkg_info</NAME>
<RETURNS>GList  **</RETURNS>
GList *all_pkgs
</FUNCTION>
<FUNCTION>
<NAME>sections</NAME>
<RETURNS>GList  *</RETURNS>
GList *pkg_sections
</FUNCTION>
<FUNCTION>
<NAME>check_missing</NAME>
<RETURNS>int  </RETURNS>
gchar *fe
</FUNCTION>
<FUNCTION>
<NAME>get_frontends</NAME>
<RETURNS>void  </RETURNS>
GtkWidget *combo
</FUNCTION>
<FUNCTION>
<NAME>fe_select</NAME>
<RETURNS>void  </RETURNS>
GtkMenuItem *entry, gpointer user_data
</FUNCTION>
<FUNCTION>
<NAME>sig_hand</NAME>
<RETURNS>void  </RETURNS>
gint sig
</FUNCTION>
<FUNCTION>
<NAME>run_config</NAME>
<RETURNS>int  </RETURNS>
gchar *cf
</FUNCTION>
<FUNCTION>
<NAME>configure_cb</NAME>
<RETURNS>void  </RETURNS>
GtkWidget *w, gpointer data
</FUNCTION>
<FUNCTION>
<NAME>write_config</NAME>
<RETURNS>int  </RETURNS>
gchar *option, gchar *data
</FUNCTION>
<FUNCTION>
<NAME>read_config</NAME>
<RETURNS>char *</RETURNS>
gchar *option
</FUNCTION>
<FUNCTION>
<NAME>remember_fe_cb</NAME>
<RETURNS>void  </RETURNS>
gpointer data, guint action, GtkWidget *w
</FUNCTION>
<FUNCTION>
<NAME>show_libgnome_missing_alert</NAME>
<RETURNS>void  </RETURNS>
gpointer data, guint action, GtkWidget *w
</FUNCTION>
<FUNCTION>
<NAME>create_main_window</NAME>
<RETURNS>GtkWidget *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>create_menubar</NAME>
<RETURNS>GtkWidget *</RETURNS>
GtkWidget *win
</FUNCTION>
<FUNCTION>
<NAME>create_gkdebconf_image</NAME>
<RETURNS>GtkWidget *</RETURNS>
gchar *iname
</FUNCTION>
<FUNCTION>
<NAME>show_about_window</NAME>
<RETURNS>void  </RETURNS>
GtkWidget *main_window, gchar *msg, gchar *iname
</FUNCTION>
<FUNCTION>
<NAME>gk_dialog</NAME>
<RETURNS>void  </RETURNS>
GtkMessageType type, gchar *msg, ...
</FUNCTION>
<FUNCTION>
<NAME>set_description</NAME>
<RETURNS>void  </RETURNS>
GtkWidget *text, gchar *pkg
</FUNCTION>
<FUNCTION>
<NAME>gtk_list_store_set_strings</NAME>
<RETURNS>void  </RETURNS>
GtkListStore *store, GList *cflist
</FUNCTION>
<FUNCTION>
<NAME>list_item_selected</NAME>
<RETURNS>void  </RETURNS>
GtkTreeSelection *selection, gpointer data
</FUNCTION>
<FUNCTION>
<NAME>cf_select</NAME>
<RETURNS>void  </RETURNS>
GtkTreeSelection *selection, gpointer data
</FUNCTION>
<FUNCTION>
<NAME>gk_splash</NAME>
<RETURNS>GtkWidget *</RETURNS>
void
</FUNCTION>
<FUNCTION>
<NAME>about_cb</NAME>
<RETURNS>void  </RETURNS>
GtkWidget *w, gpointer data
</FUNCTION>
<FUNCTION>
<NAME>gk_dialog_for_gconf</NAME>
<RETURNS>void  </RETURNS>
  gchar *key, gchar *format, ...
</FUNCTION>
